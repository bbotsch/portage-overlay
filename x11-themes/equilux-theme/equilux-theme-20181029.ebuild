# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="A Material Design-like theme for GNOME/GTK+ based desktop environments"
HOMEPAGE="https://www.opendesktop.org/p/1182169/"
SRC_URI="https://github.com/ddnexus/equilux-theme/archive/equilux-v${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64"

S="${WORKDIR}/equilux-theme-equilux-v${PV}"

src_install() {
	dodir /usr/share/themes
	./install.sh --dest "${D}/usr/share/themes" || die
}
