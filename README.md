Brandon's Portage Overlay
=========================

Welcome to my portage overlay for Gentoo Linux.

This repository contains packages I use personally that are either not abailable in the main repo or are not configured to my liking.

I have made my repo public so feel free to use it! But I make not guarantees on stability.

## Installation

This is just a quick and dirty personal overlay that I decided to make public so this overlay is not in the official list that is used by layman and eselect-repository.  
However, you can still install it using eselect-repository:
* If you have not already, install `app-eselect/eselect-repository`
* Add the repo with  
`# eselect repository add bbotsch git https://gitlab.com/bbotsch/portage-overlay.git`  
Note: You can name it anything you want, bbotsch is the name I use and will likely be the name used if I get this repo registered
* Sync the repo with  
`# emerge --sync bbotsch`
* If you use `eix`, update the cache with  
`# eix-update`
* Start using it!
